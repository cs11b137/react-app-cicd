import React from "react";
import { render, screen } from "@testing-library/react";
import { Foo } from "./App";

test("renders learn react link", () => {
  render(<Foo />);
  const linkElement = screen.getByText(/Test Ok/i);
  expect(linkElement).toBeInTheDocument();
});
